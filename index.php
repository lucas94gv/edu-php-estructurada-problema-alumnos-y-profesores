<?php

	//LOGIN
	//------------------------------------------------------------------------------------------
	/*
	//Iniciamos la sesión
	session_start();

	//var_dump($_SESSION);

	if (!isset($_SESSION['logeado'])) {
			echo "<form method='POST'>";
			echo "Usuario: <input type='text' name='usuario'><br><br>";
			echo "Contraseña: <input type='password' name='contrasena'><br><br>";
			echo "<input type='submit' name='log' value='Entrar'>";
			echo "</form>";
			echo "";
	}

	if (isset($_POST['log'])) {

		$usuario = $_POST['usuario'];
		$contrasena = $_POST['contrasena'];

		if ($usuario == "user" && $contrasena == "123") {
			$_SESSION['logeado'] = true;
		}
		else {
			echo "<p class='error'>ERROR. El usuario o la contraseña no es correcto.<p>";
		}
	}

	//------------------------------------------------------------------------------------------


	if (isset($_SESSION['logeado'])) {

		//Cargamos en sesión el array de los alumnos
		$_SESSION['alumnos'] = array(
									array("nombre" => "Amelia", "apellido" => "Garcia"),
									array("nombre" => "Senen", "apellido" => "Fernandez"),
									array("nombre" => "David", "apellido" => "Ramos"),
									array("nombre" => "Carlos", "apellido" => "Gonzalez"),
									array("nombre" => "Rosana", "apellido" => "Figueira"),
									array("nombre" => "Angela", "apellido" => "Ares"),
									array("nombre" => "Veronica", "apellido" => "Marti&ntilde;an"),
									array("nombre" => "Alberto", "apellido" => "Garcia"),
									array("nombre" => "Enma", "apellido" => "Marques"),
									array("nombre" => "Soraya", "apellido" => "Rivera")
								);

		echo "
		<html>
			<head>
				<title>Pr&aacute;ctica</title>
				<style type='text/css'>
					body {
							font-family: Courier;

							background-color: #118246;

							color: #C4EAB3;
						}
						.margenTexto {
							margin-left: 130px;
						}
						.margenAsterisco1 {
							margin-left: 140px;
						}
						.margenAsterisco2 {
							margin-left: 63px;
						}
						.error {
							color: red;
						}
				</style>
			</head>
			<body>
				<header>
					*****************************************************<br>
					* <span class='margenTexto'>Simulador Ex&aacute;menes 1.0</span><span class='margenAsterisco1'>*</span><br>
					* <span class='margenTexto'>Por favor introduzca la acci&oacute;n</span><span class='margenAsterisco2'>*</span><br>
					*****************************************************
				</header>
				<main>
					<section>
						<article>
							1- Imprimir lista de alumnos<br>
							2- A&ntilde;adir nuevo alumno<br>
							3- Simular un examen para alumnos<br>
							4- Mostrar los resultados de los ex&aacute;menes<br>
							5- Salir del programa<br><br>
						</article>
						<article>
							<form method='POST'>
								Opci&oacute;n: <input type='number' name='opcion'> <input type='submit' name='submit' value='ok'>
							</form>
						</article>
					</section>
				</main>
			</body>
		</html>
					";

	}*/


	//Iniciamos la sesión
	session_start();

	var_dump($_SESSION['alumnos']);

	//Cargamos en sesión el array de los alumnos
	$_SESSION['alumnos'] = array(
								array("nombre" => "Amelia", "apellido" => "Garcia"),
								array("nombre" => "Senen", "apellido" => "Fernandez"),
								array("nombre" => "David", "apellido" => "Ramos"),
								array("nombre" => "Carlos", "apellido" => "Gonzalez"),
								array("nombre" => "Rosana", "apellido" => "Figueira"),
								array("nombre" => "Angela", "apellido" => "Ares"),
								array("nombre" => "Veronica", "apellido" => "Marti&ntilde;an"),
								array("nombre" => "Alberto", "apellido" => "Garcia"),
								array("nombre" => "Enma", "apellido" => "Marques"),
								array("nombre" => "Soraya", "apellido" => "Rivera")
							);
?>
<html>
	<head>
		<title>Pr&aacute;ctica</title>
		<style type="text/css">
			body {
  				font-family: Courier;

  				background-color: #118246;

  				color: #C4EAB3;
  			}
			.margenTexto {
				margin-left: 130px;
			}
			.margenAsterisco1 {
				margin-left: 140px;
			}
			.margenAsterisco2 {
				margin-left: 63px;
			}
			.error {
				color: red;
			}
		</style>
	</head>
	<body>
		<header>
			*****************************************************<br>
			* <span class="margenTexto">Simulador Ex&aacute;menes 1.0</span><span class="margenAsterisco1">*</span><br>
			* <span class="margenTexto">Por favor introduzca la acci&oacute;n</span><span class="margenAsterisco2">*</span><br>
			*****************************************************
		</header>
		<main>
			<section>
				<article>
					1- Imprimir lista de alumnos<br>
					2- A&ntilde;adir nuevo alumno<br>
					3- Simular un examen para alumnos<br>
					4- Mostrar los resultados de los ex&aacute;menes<br>
					5- Salir del programa<br><br>
				</article>
				<article>
					<form method="POST">
						Opci&oacute;n: <input type="number" name="opcion"> <input type="submit" name="submit" value="ok">
					</form>
				</article>
			</section>
		</main>
	</body>
</html>
<?php
	//Si pulsamos el botón ok
	if (isset($_POST['submit'])) {

		//Almacenamos la respuesta del usuario en una variable
		$opcion = $_POST['opcion'];

		validacion($opcion);

		redireccionar($opcion);

	}

	//Función para validar la opción introducida por el usuario
	function validacion($opcion) {
		//Comprobamos que la respuesta tenga un caracter
		if (strlen($opcion) != 1) {
			echo "<p class='error'>ERROR. La opci&oacute;n solo tiene que tener un car&aacute;cter<p>";
		}
		//Comprobamos que la respuesta no sea null
		if ($opcion == null) {
			echo "<p class='error'>ERROR. La opci&oacute;n no puede ser null<p>";
		}
		//Comprobamos que la respuesta del usuario sea 1,2,3,4 o 5
		if ( ($opcion != 1) && ($opcion != 2) && ($opcion != 3) && ($opcion != 4) && ($opcion != 5) ) {
			echo "<p class='error'>ERROR. Las opciones del usuario tienen que ser 1,2,3,4 o 5<p>";
		}
	}

	//Función que según la opción que eliga el usuario redireccionar a una página o a otra
	function redireccionar($opcion) {
		if ($opcion==1) {
			header("Location: opciones/1.php");
		}
		else if ($opcion==2) {
			header("Location: opciones/2.php");
		}
	}

?>
