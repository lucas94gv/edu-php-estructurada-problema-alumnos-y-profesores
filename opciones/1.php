<?php
	//Iniciamos sesión
	session_start();

  //$_SESSION['logeado'] = true;

  var_dump($_SESSION['alumnos']);

?>
<html>
	<head>
		<title>Lista alumnos</title>
		<style type="text/css">
			body {
  				font-family: Courier;

  				background-color: #118246;

  				color: #C4EAB3;
  			}

  			hr {
  				width: 500px;
  				margin-left: 1px;
  			}

  			footer {
  				margin-top: 300px;
  			}

  			.margenApellidos {
  				margin-left: 250px;
  			}
		</style>
	</head>
	<body>
		<header>
			<hr>
			Nombre <span class="margenApellidos">Apellidos</span><br>
			<hr>
			<br>
		</header>
		<main>
			<section>
				<article>
					<?php
						//Recorremos el array de alumnos para mostrar nombre y apellido de cada uno de ellos
						foreach ($_SESSION['alumnos'] as $alumno => $value) {
							echo $value["nombre"] . " <span class='margenApellidos'>" . $value["apellido"] . "</span><br>";
						}
					?>
					<hr>
				</article>
			</section>
		</main>
	</body>
	<footer>
		<form method="POST">
			<input type="submit" value="Volver" name="volver">
		</form>
	</footer>
</html>
<?php

	//Si pulsamos el botón volver redirigimos a index.php
	if (isset($_POST['volver'])) {
		header("Location: ../index.php");
	}

?>
