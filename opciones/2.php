<?php
	//Iniciamos sesión
	session_start();

?>
<html>
	<head>
		<title>Lista alumnos</title>
		<style type="text/css">
			body {
  				font-family: Courier;

  				background-color: #118246;

  				color: #C4EAB3;
  			}

  			hr {
  				width: 500px;
  				margin-left: 1px;
  			}

  			footer {
  				margin-top: 50px;
  			}

  			.margenApellidos {
  				margin-left: 250px;
  			}

  			.error {
  				color: red;
  			}
		</style>
	</head>
	<body>
		<header>
		</header>
		<main>
			<section>
				<article>
					<form method="POST">
						<p>Introduce el nombre del alumno: <input type="text" name="nombre"></p>
						<p>Introduce el apellido del alumno: <input type="text" name="apellido"></p>
				</article>
			</section>
		</main>
	</body>
	<footer>
			     <input type="submit" value="Volver" name="volver"> <input type="submit" value="Continuar" name="continuar">
		     </form>
	</footer>
</html>
<?php

	//Si pulsamos el botón continuar redirigimos a index.php añadiendo el alumno
	if (isset($_POST['continuar'])) {

		//Almacenamos los datos introducidos por el usuario en variables
		$nombre = $_POST['nombre'];
		$apellido = $_POST['apellido'];

		validacion($nombre, $apellido);

		anhadirAlumno($nombre, $apellido);
	}

	//Función para añadir alumnos nuevos.
	//En caso de que el alumno se repita no lo añade y muestra error
	//Si el alumno es nuevo se añade al array alumnos
	function anhadirAlumno($nombre, $apellido) {

		foreach ($_SESSION['alumnos'] as $alumno => $value) {
			//Comprobamos que si ya existe un alumno con ese nombre y apellidos
			if ( $value["nombre"] == $nombre && $value["apellido"] == $apellido ) {
				echo "<p class='error'>ERROR. Ya existe un alumno con ese nombre y apellido.<p>";
				$repetido = true;
				break;
			}
		}

		//Si no se repite el alumno se añade al array
		if (!isset($repetido)) {
			$alumnoNuevo = array("nombre" => $nombre, "apellido" => $apellido);
			array_push($_SESSION['alumnos'], $alumnoNuevo);
			//var_dump($_SESSION['alumnos']);
		}
	}

	//Función para validar el nombre y apellido introducido por el usuario
	function validacion($nombre, $apellido) {
		//En caso de que alguno de los valores sea null
		if ($nombre == null || $apellido == null) {
			echo "<p class='error'>ERROR. Nombre o apellido no pueden ser null.<p>";
		}
		//En caso de que alguno de los valores tenga menos de 3 caracteres
		if (strlen($nombre)<3 || strlen($apellido)<3) {
			echo "<p class='error'>ERROR. Nombre o apellido tienen que tener 3 o m&aacute;s caracteres.<p>";
		}
	}

	//Si pulsamos el botón volver redirigimos a index.php sin añadir el alumno
	if (isset($_POST['volver'])) {
		header("Location: ../index.php");
	}

	//Si pulsamos el botón volver redirigimos a index.php añadiendo el alumno
	/*if (isset($_POST['continuar'])) {
		header("Location: ../index.php");
	}*/

  var_dump($_SESSION['alumnos']);

?>
